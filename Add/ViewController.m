//
//  ViewController.m
//  Add
//
//  Created by admin on 24/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableArray *value = [[NSMutableArray alloc]init];
    int evencount=0,oddcount=0;
    for (int i=0;i<100;i++) {
        [value addObject:@(i)];
        if(i%2==0){
            evencount++;
        
        }
        else{
            oddcount++;
        }
        
    }
    NSLog(@"Numbers:%d,%d",evencount,oddcount);
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
